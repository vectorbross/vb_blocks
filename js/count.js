(function ($, Drupal) {
    $.fn.isInViewport = function(offset) {
        if($(this).length) {
            var elementTop = $(this).offset().top;
            var elementBottom = elementTop + $(this).outerHeight();

            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();

            return elementBottom > viewportTop && elementTop < viewportBottom - (($(window).height() / 100) * offset);;
        } else {
            return false;
        }
    };

    function Counter(offset) {
        this.scrollTimer = false;
        // The offset from the bottom of the page in percentage the animated element
        // has to cross to be considered 'in view'
        this.offset = offset;
        this.init();
    }

    Counter.prototype.init = function() {
        // Check for support of IntersectionObserver
        // IE doesn't support it so we have to fall back
        // to the old setTimeout loop
        if ('IntersectionObserver' in window) {
            this.initIntersectionObserver();
        } else {
            this.initSetTimeout();
        }
    }

    Counter.prototype.initSetTimeout = function() {
        var self = this;

        this.scrollTimer = setTimeout(this.watchCounters.bind(this), 0);

        $(window).on('resize scroll show.bs.modal hide.bs.modal', function() {
            self.handleScroll();
        });
        $(document).on('ajaxSuccess ajaxError', function() {
            self.handleScroll();
        });
        $('.panel-collapse').on('show.bs.collapse', function() {
            self.handleScroll();
        });
    }

    Counter.prototype.initIntersectionObserver = function() {
        var self = this;

        const counters = document.querySelectorAll('.counter__count:not(.counter__count--finished)');
        counters.forEach(function(element) {
            // Callback which will get invoked once the element enters the viewport
            const callback = function(entries, observer) {
                // With no options provided, threshold defaults to 0 which results
                // in an array of entries storing only ONE element
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        self.initiateCounter($(entry.target));
                    }
                });
            };

            // Use rootMargin to trigger isIntersecting callback when
            // the element crosses offset bottom of the viewport 
            const io = new IntersectionObserver(callback, {rootMargin: '-' + self.offset + '% 0% -' + self.offset + '% 0%'});
            io.observe(element);
        });
    }

    Counter.prototype.watchCounters = function() {
        var self = this;
        $('.counter__count:not(.counter__count--finished)').each(function() {
            if ($(this).isInViewport(this.offset)) {
                self.initiateCounter($this);
            }
        });
        this.scrollTimer = false;
    }

    Counter.prototype.initiateCounter = function(element) {
        if(!element.hasClass('counter__count--finished')) {
            element.countTo({
                formatter: function (value, options) {
                    var split = value.toFixed(options.decimals).split('.');
                    if(split[0].length > 4) {
                        return split[0].split('').reverse().join('').replace(/([0-9]{3})/g, "$1 ").split('').reverse().join('') + (split.length > 1 ? ',' + split[1] : '');
                    } else {
                        return value.toFixed(options.decimals).replace('.', ',');
                    }
                }
            });
            element.addClass('counter__count--finished');
        }
    }

    Counter.prototype.handleScroll = function() {
        var self = this;

        if(!this.scrollTimer) {
            clearTimeout(this.scrollTimer);
            this.scrollTimer = setTimeout(this.watchCounters.bind(this), 300);
        }
    }

    Drupal.behaviors.VbParagraphsCounter = {
        attach: function (context, settings) {
            new Counter(0);
        }
    };
})(jQuery, Drupal);