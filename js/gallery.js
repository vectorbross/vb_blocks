(function ($, Drupal) {
	Drupal.behaviors.VbBlocksGallery = {
		attach: function (context, settings) {
			$(once("galleryColorbox", '.inline-block-gallery__images--grid, .inline-block-gallery__images--masonry')).each(function() {
				var id = $(this).data('id');

				$(this).find('img').each(function() {
					var img = $(this).attr('src');
					$(this).wrap('<a class="colorbox" href="'+img+'" rel="'+id+'"></a>');
				});

				$(this).find('.colorbox').colorbox({
			        maxWidth: function() {
			          if($(window).width() > 900) {
			            return $(window).width() - 160;
			          } else {
			            return $(window).width() - 80;
			          }
			        },
			        maxHeight: function() {
			          return $(window).height() - 100;
			        }
				});
			})
			$('.inline-block-gallery__images--slider').not('.slick-initialized').slick({
				infinite: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true
			});
		}
	};
})(jQuery, Drupal);