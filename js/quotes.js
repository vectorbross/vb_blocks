(function ($, Drupal) {
	Drupal.behaviors.VbBlocksQuotes = {
		attach: function (context, settings) {

			$('.inline-block-quotes--slider').not('.slick-initialized').slick({
				infinite: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true
			});

			$('.inline-block-quotes--slider-horizontal').not('.slick-initialized').slick({
				infinite: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true
			});
		}
	};
})(jQuery, Drupal);
