(function ($, Drupal) {
	Drupal.behaviors.VbBlocksBrands = {
		attach: function (context, settings) {
			$('.inline-block-brands__images--slider').not('.slick-initialized').slick({
				variableWidth: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 0,
				speed: 3000,
				cssEase: 'linear',
				arrows: false,
				infinite: true,
				pauseOnHover: false,
				pauseOnfocus: false
			}).on('beforeChange', function(event, slick, currentSlide) {
				$(this).find('.lazy-img').each(function() {
					$(this).attr('src', $(this).attr('data-src'));
					$(this).removeClass('lazy-img');
				});
    		});
		}
	};
})(jQuery, Drupal);